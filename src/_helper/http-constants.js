import axios from 'axios';

let baseURL = process.env.baseURL || "https://djivanapp.com/djivan/api";

let instance = axios.create({
	baseURL
});

export const HTTP = instance;